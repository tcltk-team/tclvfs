tclvfs (1.4.2~20230906-1) unstable; urgency=medium

  * New fossil snapshot.
  * Use separate tclconfig component together with the original tarball
    fetched from the upstream repository.
  * Refresh patches.
  * Fix cleaning after the build (closes: #1047739).
  * Bumped debhelper compatibility level to 13.
  * Bump standards version to 4.7.0.

 -- Sergei Golovan <sgolovan@debian.org>  Tue, 23 Apr 2024 12:29:58 +0300

tclvfs (1.4.2~20121213-2) unstable; urgency=medium

  * Introduced a new patch which helps with FTBFS on GNU/kFreeBSD and GNU/Hurd
    architectures.

 -- Sergei Golovan <sgolovan@debian.org>  Thu, 14 Apr 2016 23:44:26 +0300

tclvfs (1.4.2~20121213-1) unstable; urgency=medium

  * New CVS snapshot.
  * Dropped no longer necessary patches.
  * Bumped debhelper compatibility version to 9.
  * Made the binary package multi-arch aware.
  * Bumped standards version to 3.9.7.

 -- Sergei Golovan <sgolovan@debian.org>  Thu, 14 Apr 2016 21:30:27 +0300

tclvfs (1.3-20080503-4) unstable; urgency=low

  * Fixed FTBFS when building with Tcl 8.6.
  * Bumped standards version to 3.9.4.

 -- Sergei Golovan <sgolovan@debian.org>  Fri, 27 Sep 2013 09:01:43 +0400

tclvfs (1.3-20080503-3) unstable; urgency=low

  * Fixed FTBFS when cleaning before the build itself.

 -- Sergei Golovan <sgolovan@debian.org>  Fri, 01 Jun 2012 11:53:54 +0400

tclvfs (1.3-20080503-2) unstable; urgency=low

  * Renamed tclvfs into tcl-vfs to comply the Debian Tcl/Tk policy.
  * Added hardened build flags using dpkg-buildflags.
  * Use sourceforge.net redirector in debian/watch uscan control script.
  * Clarified the package description.
  * Mentioned tcllib as a suggested package in README.Debian.
  * Bumped debhelper compatibility version to 8.
  * Switched to 3.0 (quilt) source package format.
  * Bumped standards version to 3.9.3.
  * Removed TODO.Debian because Tcl 8.5 contains more recent http than 8.4.

 -- Sergei Golovan <sgolovan@debian.org>  Fri, 01 Jun 2012 09:03:31 +0400

tclvfs (1.3-20080503-1) unstable; urgency=low

  * New upstream release.
  * Mangled Debian version in debian/watch to fit the current tclvfs upstream
    versioning scheme.
  * Removed undefinrd macros BE and BS from manpages.
  * Protected quilt invocations in debian/rules to make the source package
    convertible to 3.0 (quilt) format (closes: #482712).
  * Bumped standards version to 3.8.0.

 -- Sergei Golovan <sgolovan@debian.org>  Thu, 19 Jun 2008 15:09:07 +0400

tclvfs (1.3-20070620-3) unstable; urgency=low

  * Added a patch by Victor Wagner (slightly modified) which fixes zip VFS to
    work with prepended executable.
  * Fixed default encoding and eofchar of files opened from VFS.
  * Removed empty directories from the binary package.
  * Bumped standards version to 3.7.3.

 -- Sergei Golovan <sgolovan@debian.org>  Tue, 22 Jan 2008 20:29:31 +0300

tclvfs (1.3-20070620-2) unstable; urgency=low

  * Adapted the package to Debian Tcl/Tk policy. This includes moving the Tcl
    modules into a subdirectory of /usr/share/tcltk and switching to default
    tcl package in dependencies.
  * Added Homepage field in debian/control.
  * Added uscan control file debian/watch.

 -- Sergei Golovan <sgolovan@debian.org>  Sat, 17 Nov 2007 14:27:55 +0300

tclvfs (1.3-20070620-1) unstable; urgency=low

  * New CVS snapshot.
  * Added uscan control script debian/watch.
  * Rewritten clean target in debian/rules to ignore only missing Makefile
    error.
  * Made clean-patched target in debian/rules depend on patch-stamp.

 -- Sergei Golovan <sgolovan@nes.ru>  Sun, 16 Sep 2007 21:53:34 +0400

tclvfs (1.3-20070414-2) unstable; urgency=low

  * New maintainer's email address sgolovan@debian.org.
  * Added libmemchan-tcl and libtrf-tcl to suggested packages in
    debian/control.
  * Added info about libmemchan-tcl and libtrf-tcl to README.Debian.
  * Added TODO.Debian file to documentation directory.
  * Added build-arch and build-indep targets to debian/rules.

 -- Sergei Golovan <sgolovan@debian.org>  Sat, 04 Aug 2007 21:29:12 +0400

tclvfs (1.3-20070414-1) unstable; urgency=low

  * New maintainer Sergei Golovan (closes: #383656).
  * New CVS snapshot.
  * Converted package from Debian native form to non-native form.
  * Fixed errors in manual pages.
  * Changed debhelper compatibility version to 5.

 -- Sergei Golovan <sgolovan@nes.ru>  Sun, 20 May 2007 23:25:07 +0400

tclvfs (1.3-3) unstable; urgency=low

  * QA Upload.
  * Added support for GNU/Hurd and GNU/kFreeBSD.

 -- Aurelien Jarno <aurel32@debian.org>  Sun, 10 Dec 2006 17:26:21 +0100

tclvfs (1.3-2) unstable; urgency=low

  * QA Upload (Ack NMU; Closes: #319798, #353849)
  * Set Maintainer to QA Group, Orphaned: #383656
  * Remove CVS Directories and files from tarball
  * Remove config.log on clean
  * Do not install Manpages from Makefile, use dh_installman,
    otherwise they end up in /usr/man/mann/.
  * Do not install README.cygwin
  * Conforms with latest Standards Version 3.7.2

 -- Michael Ablassmeier <abi@debian.org>  Sat,  2 Sep 2006 12:32:47 +0200

tclvfs (1.3-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * tclconfig/tcl.m4: Fixed shell syntax error. Closes: #353849.

 -- Lars Wirzenius <liw@iki.fi>  Sat, 18 Mar 2006 21:15:06 +0000

tclvfs (1.3-1) unstable; urgency=low

  * New upstream release.

 -- David N. Welton <davidw@debian.org>  Thu,  9 Dec 2004 20:33:40 +0100

tclvfs (1.2.1-3) unstable; urgency=low

  * Added Tcl dependency, tcllib Recommends, and a README.Debian file
    indicating the location of some useful extensions.
  * Bug fix: "Missing dependencies for tclvfs broke some vfs types",
    thanks to Victor Wagner (Closes: #266028).

 -- David N. Welton <davidw@debian.org>  Mon, 23 Aug 2004 20:53:11 +0200

tclvfs (1.2.1-2) unstable; urgency=low

  * Added build-depend for Tcl dev package.

 -- David N. Welton <davidw@debian.org>  Tue,  5 Aug 2003 18:47:26 +0200

tclvfs (1.2.1-1) unstable; urgency=low

  * Initial Release.

 -- David N. Welton <davidw@debian.org>  Sat, 26 Jul 2003 21:30:01 +0200

